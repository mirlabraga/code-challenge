# Questions

Q1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

Answer: The console will show 2 and 1.  The setTimeout is a function JavaScript responsible for wait a definide time for execute some function. setTimeout has two parameters, the function and the time in miliseconds. 
JavaScript used to event loop pattern for running, in this case the setTimeout doesn't need to finished for run the next function. In this case it will execute first **console.log("2");** and wait 100 miliseconds for execute **console.log("1");**

Q2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

Answer: 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0. It is a is a recursive function. When I called with argument 0, the function foo will run the **if** and check if the argument 0 is less than 10, in this case it is true and call again the function foo, but with argument 1 (0+1). So it will run until the argument be bigger than 10 then will print in the console the number and the run process exit of the if.

Q3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

Answer: Because the event loop won't have sure the line **d = d || 5;** will be run before the thrid line **console.log(d);**, so if the the **console.log(d);** the console log will print the **undefined**

Q4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

Answer: 3 .It is closure javascript implementation. The foo is a function factore, when **foo(1)** is invoked, the variable bar remains available for use, the bar is a closure and can be use again with other argument, in this case the function will add the remained value with the new argument value.

Q5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

Answer:

The function need to be used invoked two argument, the first a number and the second the function, this function in the argument needs to have 1 parameter where receive the type number. I understand the parameters is numbers in **double** function and **done** function because * represent the multiplication on JavaScript and you just can multiplication numbers for be right.
